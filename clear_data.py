import pandas as pd
import datetime
import re
import os
import config

path = config.PAST_DATA
files = [file for file in os.listdir(path) if not file.startswith('.')]


def normalize_snils(snils):
    snils = str(''.join(i for i in snils if i.isdigit()))
    while len(snils) < 11:
        snils = '0' + snils
    snils = f'{snils[0:3]}-{snils[3:6]}-{snils[6:9]} {snils[9:11]}'
    return snils


def check_snils(snils):
    snils = normalize_snils(snils)

    if len(snils) != 14:
        return pd.NaT

    def snils_csum(snils):
        k = range(9, 0, -1)
        pairs = zip(k, [int(x) for x in snils.replace('-', '').replace(' ', '')[:-2]])
        return sum([k * v for k, v in pairs])

    csum = snils_csum(snils)

    while csum > 101:
        csum %= 101
    if csum in (100, 101):
        csum = 0

    if csum == int(snils[-2:]):
        return snils
    else:
        snils = snils[:-2] + str(snils[-2:])
        return snils


def convert_snils_format(snils):
    try:
        snils = check_snils(snils)
        return snils
    except Exception as e:
        print(e)
        return pd.NaT


def convert_pass_number(n):
    try:
        number = re.sub(" ", "", str(n))
        while len(number) < 6:
            number = '0' + number
        if re.match(r'^(?!(0{6})|(\D*0{6}))\d{6}$', number):
            return number
        else:
            return pd.NaT
    except Exception as e:
        print(e)
        return pd.NaT


def convert_pass_series(d):
    try:
        series = re.sub(" ", "", str(d))
        while len(series) < 4:
            series = '0' + series
        if re.match(r'\A(?!0000)(?=\d\d\d\d)\d\d\d\d\Z', series):
            return series
        else:
            return pd.NaT
    except Exception as e:
        print(e)
        return pd.NaT


def convert_oms(s):
    try:
        oms = str(''.join(i for i in s if i.isdigit()))
        if len(oms) == 16:
            return oms
        else:
            return pd.NaT
    except:
        return pd.NaT


def clear_file():
    all_data = pd.DataFrame()
    for file in files:
        current_data = pd.read_csv(f'{path}\{file}', sep=';', encoding='utf-8')
        all_data = pd.concat([all_data, current_data])

    all_data.drop_duplicates(subset=["Имя", "Фамилия", 'Дата рождения'])
    all_data['combined'] = all_data['Имя'].astype(str) + ' ' + all_data['Фамилия'] + ' ' + all_data['Дата рождения']
    all_data.sort_index(inplace=True)

    df = pd.read_csv(config.AUTOSTART, sep=';', on_bad_lines='skip')
    df = df.drop_duplicates(subset=["Имя", "Фамилия", "Дата рождения"])
    df['combined'] = df['Имя'].astype(str) + ' ' + df['Фамилия'] + ' ' + df['Дата рождения']
    df.sort_index(inplace=True)

    clear_data = df.loc[df['combined'].isin(all_data['combined'])]
    clear_data = clear_data.drop(['combined'], axis=1)
    clear_data.sort_index(inplace=True)
    clear_data['Серия паспорта гражданина РФ'] = clear_data['Серия паспорта гражданина РФ'].apply(convert_pass_series)
    clear_data['Номер паспорта гражданина РФ'] = clear_data['Номер паспорта гражданина РФ'].apply(convert_pass_number)
    clear_data['Полис ОМС'] = clear_data['Полис ОМС'].apply(convert_oms)
    clear_data['СНИЛС гражданина (при наличии)'] = clear_data['СНИЛС гражданина (при наличии)'].apply(
        convert_snils_format)

    clear_data.replace('', pd.NaT, inplace=True)
    clear_data.dropna(
        subset=['Фамилия', 'Имя', 'Дата рождения', 'Серия паспорта гражданина РФ', 'Номер паспорта гражданина РФ'],
        inplace=True)

    name = datetime.datetime.strftime(datetime.datetime.now(), '%d.%m.%Y')

    clear_data.to_csv(rf'{config.PAST_DATA}\{name}.csv', index=False, sep=';')
    clear_data.to_csv(f'{config.PATH_FINALLY}\КЗ ЛО СМЭВ3.csv', index=False, sep=';')