import requests, random, string, os, re
from datetime import datetime, timedelta
import config

def counting_records(client_id):
    session = requests.Session()

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'}

    login_data = {
        'username': config.USERNAME,
        'password': config.PASSWORD
    }

    request = session.post(f'{config.URL}/login/', data=login_data, headers=headers)

    sql_request = f"""
        SELECT 
        count() 
    FROM 
        (SELECT DISTINCT
            arrayElement(splitByString(' ', ifNull(replaceRegexpAll(case_patient_name, '[^A-Za-zА-Яа-я ]', ''), '')), 1) AS `Фамилия`,
            arrayElement(splitByString(' ', ifNull(replaceRegexpAll(case_patient_name, '[^A-Za-zА-Яа-я ]', ''), '')), 2) AS `Имя`,
            arrayStringConcat(arraySlice(splitByString(' ', ifNull(replaceRegexpAll(case_patient_name, '[^A-Za-zА-Яа-я ]', ''), '')), 3), ' ') AS `Отчество (при наличии)`,
            CASE
                WHEN case_patient_gender = 'male' THEN '1'
                WHEN case_patient_gender = 'female' THEN '2'
                ELSE '0'
            END AS `Пол`,
            concat(substring(case_patient_birthdate, 9, 2), '.', substring(case_patient_birthdate, 6, 2), '.', substring(case_patient_birthdate, 1, 4)) as `Дата рождения`,
            replaceRegexpAll(arrayElement(splitByString(':', ifNull(case_patient_pasport_number, '')), 1), '[^0-9]', '') AS `Серия паспорта гражданина РФ`,
            replaceRegexpAll(arrayElement(splitByString(':', ifNull(case_patient_pasport_number, '')), 2), '[^0-9]', '') AS `Номер паспорта гражданина РФ`,
            concat(substring(case_patient_pasport_date_begin, 9, 2), '.', substring(case_patient_pasport_date_begin, 6, 2), '.', substring(case_patient_pasport_date_begin, 1, 4)) AS `Дата выдачи паспорта гражданина РФ`,
            concat(
             substring(replaceRegexpAll(case_patient_snils_number, '[^0-9]', ''), 1, 3), '-', 
             substring(replaceRegexpAll(case_patient_snils_number, '[^0-9]', ''), 4, 3), '-', 
             substring(replaceRegexpAll(case_patient_snils_number, '[^0-9]', ''), 7, 3), ' ',
             substring(replaceRegexpAll(case_patient_snils_number, '[^0-9]', ''), 10, 2)
            ) AS `СНИЛС гражданина (при наличии)`,
            replaceRegexpAll(case_patient_policy_unite_number, '[^0-9]', '') AS `Полис ОМС`,
            '1' as `Признак наличия диагноза из перечня`
        FROM
            iemk_diagnosis_mart
        WHERE
            (`Фамилия` != '' AND `Имя` != '') AND (`Дата рождения` != '') AND (length(`Серия паспорта гражданина РФ`) = 4) AND (length(`Номер паспорта гражданина РФ`) = 6)
            AND (`Дата выдачи паспорта гражданина РФ` != '') AND (length(`СНИЛС гражданина (при наличии)`) = 14) AND (`Полис ОМС` != '')
            AND (
                diagnosis_mkb_code4 BETWEEN 'E34.3%' AND 'E34.4%'
                OR diagnosis_mkb_code4 BETWEEN 'E66%' AND 'E77.99%'
                OR diagnosis_mkb_code4 BETWEEN 'E68%' AND 'E68.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'E70%' AND 'E90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F00%' AND 'F07.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F09%' AND 'F10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F11%' AND 'F19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F20%' AND 'F25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F28%' AND 'F30.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F31%' AND 'F31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F32%' AND 'F34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F40%' AND 'F48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F54%' AND 'F54.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F60%' AND 'F60.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'F61%' AND 'F65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F68%' AND 'F69.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F70%' AND 'F70.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F71%' AND 'F73.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F78%' AND 'F79.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F80%' AND 'F80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F98%' AND 'F98.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'F99%' AND 'F99.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G10%' AND 'G13.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'G20%' AND 'G26.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G30%' AND 'G32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G35%' AND 'G36.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G37%' AND 'G37.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G40%' AND 'G40.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G41%' AND 'G41.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G43%' AND 'G46.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G71%' AND 'G71.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G72%' AND 'G73.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G90%' AND 'G98.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H15%' AND 'H22.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'H25%' AND 'H28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H30%' AND 'H31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H34%' AND 'H35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H40%' AND 'H40.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H42%' AND 'H42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H43%' AND 'H48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H49%' AND 'H51.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H52%' AND 'H53.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H54%' AND 'H54.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H57%' AND 'H57.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H58%' AND 'H59.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H60%' AND 'H61.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'H65%' AND 'H74.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H75%' AND 'H75.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H80%' AND 'H80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H81%' AND 'H82.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H83%' AND 'H83.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H90%' AND 'H91.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H93%' AND 'H93.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H94%' AND 'H95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I01%' AND 'I09.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I10%' AND 'I15.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I20%' AND 'I25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I27%' AND 'I28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I30%' AND 'I31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I33%' AND 'I42.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'I44%' AND 'I44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I45%' AND 'I51.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I60%' AND 'I69.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I70%' AND 'I83.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I84%' AND 'I84.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I85%' AND 'I89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J12%' AND 'J18.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J30%' AND 'J34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J30%' AND 'J37.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'J38%' AND 'J39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J40%' AND 'J42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J43%' AND 'J44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J45%' AND 'J47.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J60%' AND 'J65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J66%' AND 'J70.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J80%' AND 'J80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J81%' AND 'J81.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J84%' AND 'J84.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J85%' AND 'J86.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J90%' AND 'J90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J93%' AND 'J94.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J95%' AND 'J98.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'K00%' AND 'K01.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K04%' AND 'K06.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K07%' AND 'K10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K12%' AND 'K12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K14%' AND 'K14.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K20%' AND 'K22.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K25%' AND 'K31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K40%' AND 'K46.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K50%' AND 'K63.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'K66%' AND 'K67.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K70%' AND 'K76.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K80%' AND 'K86.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K91%' AND 'K91.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K92%' AND 'K92.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L00%' AND 'L08.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L10%' AND 'L12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L40%' AND 'L41.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L51%' AND 'L54.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'L63%' AND 'L65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L80%' AND 'L80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L92%' AND 'L95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M00%' AND 'M19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M22%' AND 'M25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M30%' AND 'M35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M60%' AND 'M63.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M65%' AND 'M67.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M70%' AND 'M72.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M75%' AND 'M77.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M80%' AND 'M89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M93%' AND 'M94.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N01%' AND 'N05.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N10%' AND 'N19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N20%' AND 'N23.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N25%' AND 'N25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N30%' AND 'N30.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N32%' AND 'N32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N34%' AND 'N34.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'N39%' AND 'N39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N40%' AND 'N45.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N48%' AND 'N49.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N70%' AND 'N76.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N80%' AND 'N85.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N89%' AND 'N89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N91%' AND 'N95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'O00%' AND 'O99.99%'
                OR diagnosis_mkb_code4 BETWEEN 'P33%' AND 'P33.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q16%' AND 'Q17.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q18%' AND 'Q18.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q20%' AND 'Q21.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q22%' AND 'Q28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q30%' AND 'Q31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q32%' AND 'Q34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q38%' AND 'Q38.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q39%' AND 'Q43.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q44%' AND 'Q45.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q50%' AND 'Q52.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q55%' AND 'Q56.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q60%' AND 'Q63.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q64%' AND 'Q64.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q67%' AND 'Q67.44%'
                OR diagnosis_mkb_code4 BETWEEN 'Q75%' AND 'Q75.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q80%' AND 'Q85.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q87.1%' AND 'Q87.2%'
                OR diagnosis_mkb_code4 BETWEEN 'R32%' AND 'R32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R42%' AND 'R42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R44%' AND 'R44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R47%' AND 'R49.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R62%' AND 'R62.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S00%' AND 'S01.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S02%' AND 'S05.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S03%' AND 'S03.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S04%' AND 'S04.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S06%' AND 'S08.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S09%' AND 'S10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S12%' AND 'S12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S26%' AND 'S29.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S36%' AND 'S39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S48%' AND 'S48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S58%' AND 'S58.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S68%' AND 'S68.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S78%' AND 'S78.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S88%' AND 'S88.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T34%' AND 'T35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T36%' AND 'T65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T90%' AND 'T90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T91%' AND 'T94.99%' 
                OR diagnosis_mkb_code4 BETWEEN 'T95%' AND 'T95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T96%' AND 'T97.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Z89%' AND 'Z89.99%'
            )
            AND case_fiemk_date >= toDateTime('{datetime.strftime(datetime.now() - timedelta(30), '%Y-%m-%d 00:00:00')}')
            AND case_fiemk_date <= toDateTime('{datetime.strftime(datetime.now(), '%Y-%m-%d 00:01:00')}')
            ORDER BY `СНИЛС гражданина (при наличии)`)"""

    data = {
        'client_id': client_id,
        'database_id': '2',
        'schema': 'r47',
        'sql': sql_request
    }

    import logging

    logging.basicConfig(filename='logfile.log', level=logging.DEBUG)
    report = f'{config.URL}/superset/sql_json/'
    try:
        response = session.post(report, data=data)
    except requests.exceptions.ConnectionError as e:
        print('ConnectionError')
        exit(0)

    response = session.get(f"{config.URL}/superset/csv/{client_id}")

    number_of_entries = int(re.findall(r'\d+', response.content.decode("utf-8"))[0])

    return number_of_entries
